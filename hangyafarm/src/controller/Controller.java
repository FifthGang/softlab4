package controller;

import view.*;
import game.AntEater;
import game.Anthill;
import game.Antwatcher;
import game.Engine;
import game.Field;
import game.Food;
import game.Obstacle;
import game.Stone;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import javax.swing.Timer;


public class Controller implements ActionListener {
	private Timer timer;
	private Engine engine;
	static public long roundNumber = 0;
	Object lock;
	
	
	public Controller() {
		engine = new Engine();
		timer = new Timer(1000, this); //igy telnek a korok		
		Window.getbutton1().setText("Poison: "+Integer.toString(engine.getpoison()));	//Spryfigyelok inizializalasa
		Window.getbutton2().setText("Deo: "+Integer.toString(engine.getdeo()));
	}
	public void Start() {
		try {
			loadMapFromFile("default");
		} catch (Exception e) {
			e.printStackTrace();
		}
		roundNumber = 0;
		timer.setDelay(timer.getInitialDelay());
		this.timer.start(); 	//indulhat a jatek(a korok kezdodnek)	
	}
	
	public void sprayPoison(Field where) {
		synchronized(this){
			engine.GetPoisonSpray().SprayTo(where);
		}	
	}
	
	public void sprayDeo(Field where) {
		synchronized(this){
			engine.GetDeoSpray().SprayTo(where);
		}
	}
	
	public Engine GetEngine() { return engine; }
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		synchronized(this){
			engine.Run();
			Window.getCanvas().repaint();
			if(roundNumber % 20 == 19) {
				antEaterCome();
				int time = (int)((double)timer.getDelay()*0.87);
				timer.setDelay(Math.max(time,100));
			}
			roundNumber++;

		}
	}
	
	private void loadMapFromFile(String name) throws Exception{
		BufferedReader br = null;
        String s = null;
        int ID = 0;
        String mapFileName = "maps/"+name+".txt";
                //fajl beolvasasa	
        try 
        {
        	br = new BufferedReader(new FileReader(mapFileName));   
        } 
        catch (FileNotFoundException e) {
        	e.printStackTrace();
           throw new Exception(mapFileName + " not found.",e);
        }
                //palya letrehozasa
    	String input = null;
    	int m;
    	int n;
    	try {
			input = br.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
			br.close();
			throw new Exception("Invalid line or end of file.",e1);
		}
    	try {
			m = Integer.parseInt(input);
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
			br.close();
			throw new Exception("First line must be the horizontal size of the map",e1);
		}
    	try {
			input = br.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
			br.close();
			throw new Exception("Invalid line or end of file.",e1);
		}
    	try{
	    	try {
				n = Integer.parseInt(input);
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
				throw new Exception("Second line must be the vertical size of the map",e1);
			}
	        engine.Start(m,n);
	                //fajlbol parancsok beolvasasa (elemek lerakasa)
            while((s = br.readLine()) != null ) {   
                //Hangyaleso berakasa parancs
                if(s.equals("addAntWatcher")) {
                    try {
                             s = br.readLine();             //konzolrol ID beolvasasa
                    } catch (IOException e) {
                            e.printStackTrace();
                            throw new Exception("Invalid line or end of file.",e);
                    }
                    ID = Integer.parseInt(s);       //ID-be iras
                    
                    //Hangyaleso letrehozasa, es a megadott ID-ju palyara rakasa
                    Antwatcher antwatcher = new Antwatcher();
                    
                    for(int i = 0; i < engine.GetMap().size(); i++) {
                            if(engine.GetMap().get(i).GetId() == ID) {
                                    antwatcher.SetId(engine.GetMap().get(i).GetId());
                                    antwatcher.SetNeighbours(engine.GetMap().get(i).GetNeighbours());
                                    engine.GetMap().set(i, antwatcher);
                                    
                                    List <Field> antwNeighbours = antwatcher.GetNeighbours();
                                    for(int ii = 0; ii < 6; ii++) {
                                            int antidir = (ii < 3) ? ii+3 : ii-3;
                                            if(antwNeighbours.get(ii) != null)
                                                    antwNeighbours.get(ii).GetNeighbours().set(antidir, antwatcher);
                                    }
                                    break;
                            }
                    }
                }
                //*******************************************************************************       
                //Akadaly berakasa parancs
                else if(s.equals("addLake")) {
                    try {
                             s = br.readLine();             //konzolrol ID beolvasasa
                    } catch (IOException e) {
                            e.printStackTrace();
                            throw new Exception("Invalid line or end of file.",e);
                    }
                    ID = Integer.parseInt(s);       //ID-be iras
                    
                    //Akadaly letrehozasa, es a megadott ID-ju palyara rakasa
                    Obstacle obstacle = new Obstacle();
                    
                    for(int i = 0; i < engine.GetMap().size(); i++) {
                            if(engine.GetMap().get(i).GetId() == ID) {
                                    obstacle.SetId(engine.GetMap().get(i).GetId());
                                    obstacle.SetNeighbours(engine.GetMap().get(i).GetNeighbours());
                                    engine.GetMap().set(i, obstacle);
                                    
                                    List <Field> obsNeighbours = obstacle.GetNeighbours();
                                    for(int ii = 0; ii < 6; ii++) {
                                            int antidir = (ii < 3) ? ii+3 : ii-3;
                                            if(obsNeighbours.get(ii) != null)
                                                    obsNeighbours.get(ii).GetNeighbours().set(antidir, obstacle);
                                    }
                                    break;
                            }
                    }
                }
                //*******************************************************************************       
                //Akadaly berakasa parancs
                else if(s.equals("addAntHill")) {
                    try {
                             s = br.readLine();             //konzolrol ID beolvasasa
                    } catch (IOException e) {
                            e.printStackTrace();
                            throw new Exception("Invalid line or end of file.",e);
                    }
                    ID = Integer.parseInt(s);       //ID-be iras
                    
                    //Akadaly letrehozasa, es a megadott ID-ju palyara rakasa
                    Anthill anthill = new Anthill();
                    engine.AddUpdatable(anthill);
                    anthill.SetEngine(engine);
                    
                    for(int i = 0; i < engine.GetMap().size(); i++) {
                            if(engine.GetMap().get(i).GetId() == ID) {
                                    anthill.SetId(engine.GetMap().get(i).GetId());
                                    anthill.SetNeighbours(engine.GetMap().get(i).GetNeighbours());
                                    engine.GetMap().set(i, anthill);
                                    
                                    List <Field> anthNeighbours = anthill.GetNeighbours();
                                    for(int ii = 0; ii < 6; ii++) {
                                            int antidir = (ii < 3) ? ii+3 : ii-3;
                                            if(anthNeighbours.get(ii) != null)
                                                    anthNeighbours.get(ii).GetNeighbours().set(antidir, anthill);
                                    }
                                    break;
                            }
                    }
                }
                //*******************************************************************************       
                //Kavics berakasa parancs
                else if(s.equals("addStone")) {
                    try {
                             s = br.readLine();             //konzolrol ID beolvasasa
                    } catch (IOException e) {
                            e.printStackTrace();
                            throw new Exception("Invalid line or end of file.",e);
                    }
                    ID = Integer.parseInt(s);       //ID-be iras
                    
                    //Kavics letrehozasa, es a megadott ID-ju palyara rakasa
                    Stone stone = new Stone();                           
                    engine.GetMap().get(ID).SetStone(stone);
            }
            //*******************************************************************************       
            //Kaja berakasa parancs
            else if(s.equals("addFood")) {
                    try {
                             s = br.readLine();             //konzolrol ID beolvasasa
                    } catch (IOException e) {
                            e.printStackTrace();
                            throw new Exception("Invalid line or end of file.",e);
                    }
                    ID = Integer.parseInt(s);       //ID-be iras
                    
                    //Kaja letrehozasa, es a megadott ID-ju palyara rakasa
                    Food food = new Food(engine,engine.GetMap().get(ID));
                    
                    for(int i = 0; i < engine.GetMap().size(); i++) {
                            if(engine.GetMap().get(i).GetId() == ID) {
                                    engine.GetMap().get(i).SetFood(food);
                                    break;
                            }
                    }
                }
            }
    	}
    	catch(Exception e){ 
    		throw new Exception("Exception while reading map file",e);
        }
    	finally
    	{
    		br.close();
    	}
	}
	
	public void antEaterCome() {
		AntEater antEater = new AntEater();	
		antEater.SetEngine(this.GetEngine());
		antEater.GetEngine().AddMover(antEater);
		int ID = 155;
				
		for(int i = 0; i < engine.GetMap().size(); i++) {
			if(engine.GetMap().get(i).GetId() == ID) {
				engine.GetMap().get(i).SetAnimal(antEater);
				antEater.SetPosition(engine.GetMap().get(i));
				break;
			}
		}
	}
}

