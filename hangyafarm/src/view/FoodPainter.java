package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class FoodPainter implements Painter {

	@Override
	public void paint(Graphics g, Object what) {
		BufferedImage img = Window.getCanvas().GetImages().get("food");

		g.drawImage(img, 0, 0, null); // see javadoc for more info on the parameters        
	}

}
