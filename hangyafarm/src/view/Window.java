package view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;

import controller.Controller;

//Az ablak Singleton
public class Window  extends JFrame {
	private static final long serialVersionUID = 7847559766217654445L;
	private static MenuBar menuBar;
	private static Canvas canvas;
	private static Controller controller;
	private static Window singletonInstance;
	private static JButton b1 = new JButton("Poison");		//a ket spray figyelo 
	private static JButton b2 = new JButton("Deo");
	

	public static Window Create(){
		if(singletonInstance == null)
			return new Window();
		else return null;
		
	}
	protected Window() {
		super("FifthGang presents Hangyafarm");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		controller = new Controller();
		menuBar = new MenuBar();
		b1.setEnabled(false);
		b2.setEnabled(false);
		menuBar.add(b1);
		menuBar.add(b2);
		canvas = new Canvas();
		setJMenuBar(menuBar);
		add(canvas);
		setResizable(false);
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		createBufferStrategy(2);
	}
	static Controller GetController() { return controller; }
	
	
	static BufferedImage loadImageFromFile(String name){
		 BufferedImage img = null;
		 
		 try {                
	          img = ImageIO.read(new File("images\\"+name+".png"));
	       } catch (IOException ex) {
	            // handle exception...
	       }

        return img;
	}
	
	public static Canvas getCanvas() {
		return canvas;
	}
	public static JButton getbutton1(){		//getter a spray figyeloknek
		return b1;
	}
	public static JButton getbutton2(){
		return b2;
	}
}
