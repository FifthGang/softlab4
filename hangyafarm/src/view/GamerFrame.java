package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;





public class GamerFrame extends JFrame {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 4414970127295807259L;


	int myScore;
	
    private GamerData data;
    private boolean isRunning=true;
     
 
    private void initComponents() {
        this.setLayout(new BorderLayout());
        JScrollPane scrpanel=new JScrollPane();
        JPanel newpanel=new JPanel();
        
        JTextField bevitel1=new JTextField(20);           
        JLabel cimke1=new JLabel("Name:");
        JButton gomb=new JButton("Felvesz");
        listen l=new listen(bevitel1);
        gomb.addActionListener(l);
        
        JTable tabel=new JTable();
        tabel.setFillsViewportHeight(isBackgroundSet());
        tabel.setModel(data);
        
        
        
        scrpanel.getViewport().add(tabel,BorderLayout.CENTER);
       
        if(isRunning) //abban az esetben, ha GameOver volt(isRunning=true), beviheti a felhaszn�l� a nev�t, am�gy csak kiolvashatja
        {
        newpanel.add(cimke1);
        newpanel.add("N�v",bevitel1);
        newpanel.add(gomb);
        }
        
        this.add(scrpanel);
        this.add(newpanel,BorderLayout.SOUTH);
       
       
        // ...
    }
    public class listen implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
		data.addGamer(elso.getText(), myScore);
		}
			private JTextField elso;
			
			public listen(JTextField elso) {
				this.elso=elso; 
			}
	}

    /*
     * Az ablak konstruktora.
     * 
     * NE M�DOS�TSD!
     */
    @SuppressWarnings("unchecked")
    public GamerFrame(boolean isRunning, int score) {
        super("ScoreTable");
        
        this.isRunning=isRunning;
		 this.myScore=score;
        // Indul�skor bet�ltj�k az adatokat
        try {
            data = new GamerData();
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("gamers.dat"));
            data.gamers = (List<Gamer>)ois.readObject();
            ois.close();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        
        // Bez�r�skor mentj�k az adatokat
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("gamers.dat"));
                    oos.writeObject(data.gamers);
                    oos.close();
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        // Fel�p�tj�k az ablakot
        setMinimumSize(new Dimension(500, 200));
        initComponents();
    }

   
	
	/*public static void main(String[] args) {
		 GamerFrame sf = new GamerFrame();
	        sf.setVisible(true);

	}*/

}
