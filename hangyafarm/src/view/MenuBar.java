package view;

import controller.*;
import java.awt.event.*;

import javax.swing.*;

public class MenuBar extends JMenuBar{
	private static final long serialVersionUID = 5238742618155251717L;
	JButton newGameMenu;
	JButton showScoretableMenu;
	JButton resetGameMenu;
	JButton exitMenu;
	
	public MenuBar() {
		super();
		newGameMenu = new JButton("New Game");
		showScoretableMenu= new JButton("Show Scoretable");
		resetGameMenu= new JButton("Reset Game");
		exitMenu= new JButton("Exit");
		
		newGameMenu.addActionListener(new NewGameActionListener());
		exitMenu.addActionListener(new ExitActionListener());
		showScoretableMenu.addActionListener(new ShowScoretableActionListener());
		resetGameMenu.addActionListener(new ResetGameActionListener());
		
		this.add(newGameMenu);
		this.add(showScoretableMenu);
		this.add(resetGameMenu);
		this.add(exitMenu);
		
		
		validate();
	}
	
	public class NewGameActionListener implements ActionListener {		
		public void actionPerformed(ActionEvent e) {
			if(Window.GetController() != null) {
				Window.GetController().GetEngine().Reset();
				Window.GetController().Start();
			}
		}		
	}
	
	public class ShowScoretableActionListener implements ActionListener {		
		public void actionPerformed(ActionEvent e) {
			GamerFrame frame = new GamerFrame(true,-1);
			frame.setVisible(true);
		}
	}
	
	public class ResetGameActionListener implements ActionListener {		
		public void actionPerformed(ActionEvent e) {
			if(Window.GetController().GetEngine().IsRunning()) {
				if(Window.GetController() != null) {
					Window.GetController().GetEngine().Reset();
					Window.GetController().Start();
				}
			}
		}		
	}
	
	public class ExitActionListener implements ActionListener {		
		public void actionPerformed(ActionEvent e) {
			System.exit(-1);
		}		
	}
}
