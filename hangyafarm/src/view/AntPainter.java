package view;

import javax.imageio.*;
import game.*;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class AntPainter implements Painter {

	@Override
	public void paint(Graphics g, Object what) {
		Ant ant = (Ant)what;
		BufferedImage img = null;
		
		switch(ant.GetDirection()) {
		case 0: img =  Window.getCanvas().GetImages().get("ant_0"); break;
		case 1: img =  Window.getCanvas().GetImages().get("ant_1"); break;
		case 2: img =  Window.getCanvas().GetImages().get("ant_2"); break;
		case 3: img =  Window.getCanvas().GetImages().get("ant_3"); break;
		case 4: img =  Window.getCanvas().GetImages().get("ant_4"); break;
		case 5: img =  Window.getCanvas().GetImages().get("ant_5"); break;
		}
		
		g.drawImage(img, 0, 0, null); // see javadoc for more info on the parameters        
	}

}
