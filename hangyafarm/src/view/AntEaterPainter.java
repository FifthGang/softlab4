package view;

import game.AntEater;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class AntEaterPainter implements Painter {

	@Override
	public void paint(Graphics g, Object what) {
		AntEater antEater = (AntEater)what;
		
		
		BufferedImage img = null;
		
		switch(antEater.GetDirection()) {
		case 0: img =  Window.getCanvas().GetImages().get("anteater_0"); break;
		case 1: img =  Window.getCanvas().GetImages().get("anteater_1"); break;
		case 2: img =  Window.getCanvas().GetImages().get("anteater_2"); break;
		case 3: img =  Window.getCanvas().GetImages().get("anteater_3"); break;
		case 4: img =  Window.getCanvas().GetImages().get("anteater_4"); break;
		case 5: img =  Window.getCanvas().GetImages().get("anteater_5"); break;
		}
		
		g.drawImage(img, 0, 0, null); // see javadoc for more info on the parameters        
	}

}
