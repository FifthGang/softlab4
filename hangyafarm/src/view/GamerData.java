package view;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;




public class GamerData  extends AbstractTableModel {
	
    
	List<Gamer> gamers = new ArrayList<Gamer>();
    
    public boolean isCellEditable(int i,int ii){
    	return true;
    }
    public void setValueAt(Object obj,int row,int culomn){
    	switch(culomn){
    	case 0:gamers.get(row+1).setName(obj.toString());
    	case 1:gamers.get(row+1).setScore((Integer)obj);
    	
    	}
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        Gamer gamer = gamers.get(rowIndex);
        switch(columnIndex) {
            case 0: return gamer.getName();
            default: return gamer.getScore();
           
        }
    }
    
    public int getColumnCount(){
    	return 2;
    }
    public int getRowCount(){
    	return gamers.size();
    }
    public String getColumnName(int i){
    	
    	switch(i){
    	case 0: return "Name";
    	case 1: return "Score";
 
    	}
		return null;
    }
    public Class getColumnClass(int i){
    	return this.getValueAt(0, i).getClass();
    }
    
    public void addGamer(String name, Integer score) {
    	Gamer e=new Gamer(name, score);
    	gamers.add(e);
    	this.fireTableDataChanged();
    }

}
