package view;
import java.io.Serializable;


public class Gamer implements Serializable {

	 private String name;
	 private Integer score;
	 
	 
public Gamer(String name, Integer score) 
{
	this.name = name;
	this.score = score;
}


public void setName(String name) {
    this.name = name;
}

public String getName() {
    return name;
}

public void setScore(Integer score) {
    this.score = score;
}

public Integer getScore() {
    return score;
}



}