package game;

public class Obstacle extends Field {

        public boolean CanStepOn(Ant p_ant) {
        	return false;
        }
        
        public boolean CanStepOn(AntEater p_anteater) {
        	return false;
        }

        public void StepOn(Animal p_animal) {

        }

        public void StepOff() {

        }

        public Food GetFood() {
        	return null;
        }


        public void SetAntSmell(AntSmell p_antsmell) {
        }

        public void SetFoodSmell(FoodSmell p_foodsmell) {

        }

        public void SetPoison(Poison p_poison) {
        }
        
        public void SetFood() {

        }

		@Override
		public void SetFood(Food p_food) {
			
		}

		@Override
		public void SetStone(Stone p_stone) {
			
		}

		@Override
		public Stone GetStone() {
			return null;
		}
}