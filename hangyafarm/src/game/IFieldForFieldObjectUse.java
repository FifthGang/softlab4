package game;

import java.util.List;

public interface IFieldForFieldObjectUse {

        public void SetFood(Food p_food);

        public void SetAntSmell(AntSmell p_antsmell);
        
        public FoodSmell GetFoodSmell();

        public void SetFoodSmell(FoodSmell p_foodsmell);

        public void SetPoison(Poison p_poison);
        
        /**
         * Visszaadja a mez� szomsz�dait.
         * @return Az aktu�lis mez� szomsz�dai, list�ban t�rolva.
         * @author Szak�llas D�vid
         */
        public List<? extends IFieldForFieldObjectUse> GetNeighbours();

        public Animal GetAnimal();
        
        public Food GetFood();

        public void SetAnimal(Animal p_animal);
        
        public Poison GetPoison();
        
        public void SetStone(Stone p_stone);
        
        public Stone GetStone();
}