package game;

public class FoodSmell extends FieldObject{
	/**
     * Konstruktor a <code>FoodSmell</code> inicializ�l�s�hoz.
     * @param p_engine A j�t�kmotor referenci�ja.
     * @param p_position Az a mez�, ahol az objektum l�trej�n.
     * @author Szak�llas D�vid
     */
    FoodSmell(IEngineForIngameUse p_engine, IFieldForFieldObjectUse p_position) {
    	super(p_engine, p_position);
    	position.SetFoodSmell(this);
	}

	private int intensity;

    public void Add(int p_amount) {
    	this.intensity+=p_amount;		// Intenzitas novelese a megadott ertekkel
    }

    public void Sub(int p_amount) {
    	if(this.intensity-p_amount>=0)	// Intenzitas csokkentese a megadott ertekkel
    		this.intensity-=p_amount;	// Ne lehessen negativ ertek!
    	else
    		this.intensity=0;
    }

    public int GetIntensity() {
    	return this.intensity;			// Getter metodus az intenzitashoz
    }
}
