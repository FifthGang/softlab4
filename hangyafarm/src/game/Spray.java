package game;

public abstract class Spray {
        protected int amount;  	    //itt taroljuk a Spray muniiojat
        protected boolean isEmpty;  //itt taroljuk, hogy hasznalhato-e meg a spray, vagy kifogyott-e, ha igen, akkor tobbi hasznalhatatlan  
        
        

        public abstract void Refill();
        public abstract void SprayTo(IFieldForSprayUse p_where);
}