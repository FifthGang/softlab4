package game;

import java.util.List;

public class EmptyField extends Field {
    private Food food;
    private Stone stone;
    
    public EmptyField() {	//kezdeti ertekek beallitasa
    	food = null;
    	this.SetStone(null);
    	
    }

    public boolean CanStepOn(Ant p_ant) {
    	if(this.GetAnimal() == null && this.GetStone() == null) {
    		return true; //nincs itt se allat, se kavics, lehet ide lepni
    	}
    	else {
    		return false; //itt van egy kavics, vagy allat, nem lehet idelepni
    	}
    }
    
    public boolean CanStepOn(AntEater p_anteater) {
    	int dir = p_anteater.direction; //hangyaszsun iranya
		int antidir = (dir >= 3) ? (dir-3) : (dir+3); //iranyaval ellentetes irany
		List<Field> neighbours = this.GetNeighbours(); //aktualis szomszedok lekerdezese
		List<Field> neighbours2 = null; //majd kesobb ebbe mentjuk le a hangyaszsun iranyaba eso szomszedot
		
		
		if(this.GetAnimal() == null && this.GetStone() == null && this.GetFood() == null) {
    		return true; //nincs itt se allat, se kavics, lehet ide lepni 
    	}
		else if(this.GetAnimal() != null) {
    		return false; //itt van egy allat, nem lehet idelepni
		}
		else if(this.GetFood() != null) {
    		return false; //itt van egy allat, nem lehet idelepni
		}
		else {   //kavics van itt, megnezzuk hogy el lehet-e tolni
			
    		if(neighbours.get(dir) == null) { 
    			return false; //ha vege a palyanak, akkor nem tud csuszni a kavics, tehat akadaly lesz
    		}
    		else 
    			if(neighbours.get(antidir)==null) {return false;}
    			neighbours2 = neighbours.get(antidir).GetNeighbours(); //lekerjuk a hangyaszsun iranyaval ellentetes iranyba eso szomszedot
    		
    		
    		//Ha a hangyaszsun az elozo, vagy elozo elotti helyen megtalalhato, es a kovetkezo mezore ra lehet lepni, akkor eltud csuszni a kavics (akar ketto is)
        	if(((p_anteater.position == neighbours.get(antidir) || 
        				(p_anteater.position == neighbours2.get(antidir))))
        				&& (neighbours.get(dir).CanStepOn(p_anteater) == true)) {
        			return true;
        	}
    		else {	//nem tud(nak) elcsuszni a kavics(ok)
    			return false;
    		}
		
    			
		
		}
    }
    
    public void StepOn(Animal p_animal) {	
    	if(this.GetStone() != null) { //ha van itt kavics
        	List<Field> neighbours = this.GetNeighbours(); //sajat szomszedok lekerdeze
        	List<Field> neighbours2 = neighbours.get(p_animal.direction).GetNeighbours(); //hangyaszsun iranyaval egybeeso szomszed szomszedai
        	
            if(neighbours.get(p_animal.direction).GetStone() != null) { //akkor tudjuk, hogy ott is van egy kavics amit elkell tolni
            	neighbours2.get(p_animal.direction).SetStone(neighbours.get(p_animal.direction).GetStone());//eloszor a masik kavicsot csusztatom
            	neighbours.get(p_animal.direction).SetStone(this.GetStone());//a masik kavics helyere atrakom az en kavicsom
            	this.SetStone(null);//kitorlom az en kavicsom
            	
            	this.SetAnimal(p_animal); //johet a hangyaszsun
            }
            else { //akkor tudjuk hogy csak a rajtunk levo kavics van
            	neighbours.get(p_animal.direction).SetStone(this.GetStone());
            	this.SetStone(null);//kit�rl�m az atcsusztatott kavicsot innen
            	
            	this.SetAnimal(p_animal); //johet a hangyaszsun
            }
    	}
    	else { //nincs kavics, siman ram lephet
        	this.SetAnimal(p_animal);

			if(this.GetPoison() != null) //ha hangya lepett ide, es mergezett volt a terulet, akkor meghal
				p_animal.Kill();
    	}
    }

    public void StepOff() {
    	this.SetAnimal(null); //leveszem magamrol az allatot
    }

    public Food GetFood() {
    	return food; //ha van food a mezon akkor visszaadom, ha nincs akkor null-t adok vissza
    }

    public void SetFood(Food p_food) {
            this.food = p_food; //food beallitasa
    }

	
	public void SetStone(Stone p_stone) {
	stone = p_stone;
	}

	@Override
	public Stone GetStone() {
		return stone;
	} 
}