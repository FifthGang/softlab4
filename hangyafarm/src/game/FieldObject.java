package game;

public abstract class FieldObject {
        protected IFieldForFieldObjectUse position;
        protected IEngineForIngameUse engine;
        
        /**
         * Konstruktor a <code>FieldObject</code> inicializ�l�s�hoz.
         * @param p_engine A j�t�kmotor referenci�ja.
         * @param p_position Az a mez�, ahol az objektum l�trej�n.
         * @author Szak�llas D�vid
         */
        FieldObject(IEngineForIngameUse p_engine, IFieldForFieldObjectUse p_position)
        {
        	engine = p_engine;
        	position = p_position;
        }
}