package game;

public interface IEngineForIngameUse {

        public void DeleteMover(IMovable p_mover);

        public void DeleteFood(Food p_food);

        public void DeleteUpdatable(IUpdatable p_updatable);

        public void AddMover(IMovable p_mover);

        public void AddUpdatable(IUpdatable p_updatable);
        
        public void AddFood(Food p_food);
}