package game;

import java.util.List;

public interface IFieldForSprayUse {
		
    	/**
    	 * Visszaadja a mez� szomsz�dait.
    	 * @return Az aktu�lis mez� szomsz�dai, list�ban t�rolva.
    	 * @author Szak�llas D�vid
    	 */
        public List<? extends IFieldForSprayUse> GetNeighbours();

        public void DeleteAntSmell();

        public void CreatePoison();
}