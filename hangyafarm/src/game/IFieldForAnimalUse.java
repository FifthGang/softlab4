package game;

import java.util.List;

public interface IFieldForAnimalUse {

	    /**
	     * Visszaadja a mez� szomsz�dait.
	     * @return Az aktu�lis mez� szomsz�dai, list�ban t�rolva.
	     * @author Szak�llas D�vid
	     */
        public List<? extends IFieldForAnimalUse> GetNeighbours();

        public boolean CanStepOn(Ant p_ant);
        
        public boolean CanStepOn(AntEater p_anteater);

        public int GetAntIntensity();

        public int GetFoodIntensity();

        public void StepOn(Animal p_animal);

        public void StepOff();

        public Food GetFood();

        public Animal GetAnimal();
        
        public void SetAnimal(Animal p_animal);

        public void CreateAntSmell();
        
        public void SetFood(Food food); //only for skeleton
        
        public AntSmell GetAntSmell();
        
        public long GetId();
        
        		
}