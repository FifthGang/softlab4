package game;

public class Poison extends FieldObject implements IUpdatable {
	//T�R�LNI KELL!!! az�rt hagytam benne hogy leforduljon. D�vid
	Poison() {
		super(null,null);
		this.intensity=5;
	}
	/**
     * Konstruktor a <code>Poison</code> inicializ�l�s�hoz.
     * @param p_engine A j�t�kmotor referenci�ja.
     * @param p_position Az a mez�, ahol az objektum l�trej�n.
     * @author Szak�llas D�vid
     */
	Poison(IEngineForIngameUse p_engine, IFieldForFieldObjectUse p_position) {
		
		super(p_engine, p_position);
		position.SetPoison(this);
		this.intensity=5;     //kezdeti �rt�k be�ll�t�sa
		//elvileg nem fordulhat elo, de sosem art egy ellenorzes
		if(position.GetPoison() != null){
			engine.AddUpdatable(this);
		}
    }

	private int intensity;
	
	// Elvileg nem lehet olyan eset, hogy 0 az intenzitas, es megis letezik az objektum,
	// de nem art megvizsgalni
    public void Update() {
    	if(this.intensity > 0) {				// Ha van mereg, akkor megnezzuk, hogy van-e allat a mezon
	    	if(position.GetAnimal()!=null) {	// Ha van allat a mezon,
	    		position.GetAnimal().Kill();	// majd meghivjuk a kill fuggvenyet
	    	}
    	}
    	decrement();
    	if(this.intensity==0) {					// Ha az intenzitas nulla, toroljuk a merget
    		engine.DeleteUpdatable(this);		// Az Uptadable-k kozul
    		position.SetPoison(null);			// es az adott position-rol is
    	}
    }

    public void SetToMax() {
        this.intensity=5;
    }

    private void decrement() {
    	if(this.intensity>=1)			// Nem lehet negativ!
    		this.intensity--;
    }
    
	public long GetIntensity() {
		return intensity;
	}
}