package game;

import java.util.List;

public class DeoSpray extends Spray {
	
		public DeoSpray(int am) {  //konstruktor  
			this.amount = am;   //a municio kezdeti erteke
			this.isEmpty= false; //letrehozaskor false az erteke, azaz hasznalhato, ha egyszer kifogy, akkor tobbe nem hasznalhato
		}
		

        public void Refill() {   //felelos a dezodor mennyisegenek novelesere, ha az megengedett
            if(this.isEmpty= false) {  //ha false, akkor az azt jelenti, hogy haszn�lhat�, azaz �jrat�lthet� is   
        	
            	this.amount += 5;//a dezodor mennyiseget megnoveli 5 egyseggel 
            }
        }

        public void SprayTo(IFieldForSprayUse p_where) {
        	   if(this.isEmpty==false) {  //ha az isEmpty false, azt jelenti, hogy van meg municio, lehet fujni
           		
        		   if(amount==0) {  //ha a csokkentessel a municio 0 lett, akkor tobbe nem hasznalhato 
            		   this.isEmpty =true;  //isEmpty-t true-ra allitjuk ezzel tobbe nem lesz ujboltoltheto es nem elhet tobbet fujni
            	   }      	   
        		   this.amount -= 1; //csokkenti a dezodor municiojat. 
        		   p_where.DeleteAntSmell(); //a parameterkennt kapott mezon torli a hangyaszagot
               
        		   List<? extends IFieldForSprayUse> neighbours = p_where.GetNeighbours();
        		   for(int i=0;i<neighbours.size();i++){
        			   if(neighbours.get(i) != null) {
        			   neighbours.get(i).DeleteAntSmell(); }//a parameterkent kapott mezo szomszedain is kitorli a hangyaszagot
        		   }
        	   }
        }
}