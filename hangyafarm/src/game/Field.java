package game;

import java.util.List;

public abstract class Field implements IFieldForAnimalUse, IFieldForSprayUse, IFieldForFieldObjectUse {
        
		private List<Field> neighbours;
        private Animal animal;
        private IEngineForIngameUse engine;
        private Poison poison;
        private AntSmell antSmell;
        private FoodSmell foodSmell;

        protected long id;


        public long GetId() {
        	return id;
        }
        public void SetId(long p_id) {
        	id = p_id;
        }
        /**
         * Visszaadja a mez� szomsz�dait.
         * @return Az aktu�lis mez� szomsz�dai, list�ban t�rolva.
         * @author Szak�llas D�vid
         */
        public List<Field> GetNeighbours() {
        	//visszaadjuk a szomsz�dokat
        	return neighbours;
        }
        /**
         * Visszaadja a mez�n l�v� hangyaszag er�ss�g�t.
         * @return Az aktu�lis mez�n l�v� hangyaszag er�ss�ge, 32 bites eg�szk�nt.
         * @author Szak�llas D�vid
         */
        public int GetAntIntensity() {
        	//ha nincsen hangyaszag, az nullanak felel meg
        	if(antSmell == null) return 0;
        	else
        		//lekerjuk a hangyaszag intenzitasat
        		return antSmell.GetIntensity();
        	
        }
        /**
         * Visszaadja a mez�n l�v� �llat referenci�j�t.
         * @return Az aktu�lis mez�n l�v� �llat referenci�ja, vagy <code>null</code>, ha nincs ilyen.
         * @author Szak�llas D�vid
         */
        public Animal GetAnimal() {
            return this.animal;
        }
        /**
         * Visszaadja a mez�n l�v� kajaszag er�ss�g�t.
         * @return Az aktu�lis mez�n l�v� kajaszag er�ss�ge, 32 bites eg�szk�nt.
         * @author Szak�llas D�vid
         */
        
        public FoodSmell GetFoodSmell() { return foodSmell; }
        
        public int GetFoodIntensity() {
        	//ha nincsen kajaszag, az nullanak felel meg
        	if(foodSmell == null) return 0;
        	else
        		//lekerjuk a kajaszag intenzitasat
        		return foodSmell.GetIntensity();
        	
        	}
        /**
         * Ez a met�dus a param�terek�nt adott <code>p_neighbours</code>-ra �ll�tja a mez� szomsz�dait.
         * @param p_neighbours Referencia a be�ll�tand� szomsz�dokat tartalmaz� list�ra.
         * @author Szak�llas D�vid
         */
        public void SetNeighbours(List<Field> p_neighbours) {
        	this.neighbours = p_neighbours;
        }
        /**
         * Ez a met�dus a param�terk�nt adott <code>p_antsmell</code>-re �ll�tja a mez� hangyaszag�t.
         * @param p_antsmell Referencia a be�ll�tand� hangyaszagra.
         * @author Szak�llas D�vid
         * @see IFieldForFieldObjectUse
         */
        public void SetAntSmell(AntSmell p_antsmell) {
        	this.antSmell=p_antsmell;
        }
        /**
         * Ez a met�dus a param�terk�nt adott <code>p_poison</code>-ra �ll�tja a mez� m�regtartalm�t.
         * @param p_poison Referencia a be�ll�tand� m�regtartalomra.
         * @author Szak�llas D�vid
         * @see IFieldForFieldObjectUse
         */
        public void SetPoison(Poison p_poison) {
        	poison = p_poison;
        }
        /**
         * Ez a met�dus a param�terk�nt adott <code>p_foodsmell</code>-re �ll�tja a mez� kajaszag tartalm�t.
         * @param p_foodsmell Referencia a be�ll�tand� kajaszagra.
         * @author Szak�llas D�vid
         * @see IFieldForFieldObjectUse
         */
        public void SetFoodSmell(FoodSmell p_foodsmell) {
        	this.foodSmell = p_foodsmell;
        }
        /**
         * Ez a met�dus a param�terk�nt adott <code>p_animal</code>-t r�helyezi a mez�re.
         * @param p_foodsmell Referencia az �llatra.
         * @author Szak�llas D�vid
         * @see IFieldForFieldObjectUse
         */
        public void SetAnimal(Animal p_animal) {
        	this.animal = p_animal;
        }
        /**
         * L�trehoz egy hangyaszag objektumot, ha sz�ks�ges.
         * @author Szak�llas D�vid
         */
        public void CreateAntSmell() {
        	if(antSmell != null)
        		//m�r van rajtunk smell, maximumra �ll�tjuk.
        		antSmell.SetToMax();
        	else
        		new AntSmell(engine,this);
        }
        /**
         * Let�rli a mez�r�l a hangyaszagot, ha van rajta.
         * @author Szak�llas D�vid
         */
        public void DeleteAntSmell() {
                if(this.antSmell != null) {
                	antSmell.Delete();
                }
        }
        /**
         * L�trehoz egy m�reg objektumot, ha sz�ks�ges.
         * @author Szak�llas D�vid
         */
        public void CreatePoison() {
            if(this.poison==null) {
            	poison = new Poison(engine,this);
            }
            else { 
            	//m�r van rajtunk poison, maximumra �ll�tjuk.
            	poison.SetToMax();
            }     	 
        }
        /**
         * Visszaadja a mez�n l�v� m�regre mutat� referenci�t, ha van.
         * @author Szak�llas D�vid
         * @return A mez�n l�v� <code>Poison</code>-ra mutat� referencia. Ha nincs, akkor <code>null</code>.
         */
        public Poison GetPoison() {
        	if(this.poison != null)
        		return this.poison;
        	else
        		return null;
        	
        }
        
        public AntSmell GetAntSmell() {
        	if(this.antSmell != null)
        		return this.antSmell;
        	else
        		return null;
        	
        }
        
        public abstract void SetStone(Stone p_stone);
        
        public abstract Stone GetStone();
        /**
         * Ez a met�dus a param�terk�nt adott <code>p_food</code>-ra �ll�tja a mez� kajatartalm�t.
         * @param p_food Referencia a be�ll�tand� kaj�ra.
         * @author Szak�llas D�vid
         * @see IFieldForFieldObjectUse
         */
        public abstract void SetFood(Food p_food);
        public abstract boolean CanStepOn(Ant p_ant);
        
        public abstract boolean CanStepOn(AntEater p_anteater);

        public abstract void StepOn(Animal p_animal);

        public abstract void StepOff();

        public abstract Food GetFood();
        
        ///// CSAK TESZTEL�SHEZ - MAJD T�R�LNI /////
        public void SetEngine(Engine engine) {
        	this.engine = engine;
        }
        public IEngineForIngameUse GetEngine() {
        	return this.engine;
        }
}