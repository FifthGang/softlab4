package game;

public class Antwatcher extends Field {

        public boolean CanStepOn(Ant p_ant) { //megadja minden hangyanak, hogy ralephet
        	
        	return true;   //mivel a visszateresi ertek true, ezert a hangyak ra fognak lepni
        }
        
        public boolean CanStepOn(AntEater p_anteater) { //megadja az AntEaternek, hogy r�l�phet
        	
        	return true;  //mivel a visszateresi ertek true, ezert a hangyaszsun ra fog lepni
        }

        public void StepOn(Animal p_animal) { //a parameterkent kapott allatot az Antwatcher beregisztralja(beirja a referenciajat)
        	
        	this.SetAnimal(p_animal); //megtortenik a beregisztralas
        	
        	p_animal.Kill();   //a beregisztalt allatnak meghivja a Kill fuggvenyet, ha az egy hangya akkor meg fog halni
        }        
        public void StepOff() {
        	//ez a metodus egyszeruen torli a mezorol az adott allatot
        	this.SetAnimal(null);  //megtortenik a mezon levo allatt torlese. 
        }

        public Food GetFood() {
         	//a Field ososztaly ezen fuggvenye visszaadja a mezon talalhato etelt, ha van. 
        	return null;   //Ertelemszeruen itt mindig null fog visszaadodni, mert Antwatcher mezon nem lehet kaja.
        }

		@Override
		public void SetFood(Food p_food) {	
		}

		@Override
		public void SetStone(Stone p_stone) {		
		}

		@Override
		public Stone GetStone() {
			return null;
		}

        public void SetPoison(Poison p_poison) {            
        }
}
        