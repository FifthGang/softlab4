package game;

import java.util.ArrayList;
import java.util.List;

public class Food extends FieldObject {
	/**
     * Konstruktor a <code>Food</code> inicializ�l�s�hoz.
     * @param p_engine A j�t�kmotor referenci�ja.
     * @param p_position Az a mez�, ahol az objektum l�trej�n.
     * @author Szak�llas D�vid
     */
	private List<FoodSmell> smell = new ArrayList<FoodSmell>();		// Szagokat tarolo lista.
	
    public Food(IEngineForIngameUse p_engine, IFieldForFieldObjectUse p_position) {
    	super(p_engine, p_position);
    	position.SetFood(this);
    	// vizsgalat azert, mert lehet hogy a gyoker felhasznalo nem EmptyFieldre tette:
    	// reszben ezert jo hogy itt allitjuk be mindket referenciat.
    	if(position.GetFood()!= null){
    		engine.AddFood(this);
    	}
    	
    	 List<? extends IFieldForFieldObjectUse> neighbours = position.GetNeighbours();
    	 
    	 for(int i=0;i<6;i++) {	// Uj kaja letrehozasa eseten letrehozunk hat uj kajaszagot
    		 if(neighbours.get(i)!=null){	 
    			 if(neighbours.get(i).GetFoodSmell() != null) {	// Ha az adott szomszedon mar van kajaszag
	    			neighbours.get(i).GetFoodSmell().Add(5);
	    			smell.add(neighbours.get(i).GetFoodSmell());	// akkor azt lekerem, es azt rakom be a smell listaba
	    		 }	// es ehhez a mar meglevo FoodSmellhez hozzaadok meg 5-ot (tetszoleges ertek)
	    		 else{	// Ha nem volt rajta kajaszag, akkor ujat hozok letre
	    			 FoodSmell tempSmell= new FoodSmell(engine,neighbours.get(i));
	    			 neighbours.get(i).SetFoodSmell(tempSmell);
	    			 smell.add(tempSmell);	// Az ujonnan letrehozott kajaszagot hozzaadom a smell listahoz
	    		 }
    		 }
    	 }
    }
    public void Consume() {
    	for(int i=0;i<smell.size();i++) {			// Vegig megyunk a listan,
   	 		smell.get(i).Sub(5);		// es minden szagbol kivonunk otot.(tetszoleges ertek)
   	 		if(smell.get(i).GetIntensity()==0) {	// Ha az uj ertek igy nulla, 
   	 			smell.get(i).position.SetFoodSmell(null);	// akkor toroljuk az adott mezorol a kajaszagot.
   	 		}
   	 	}
    	engine.DeleteFood(this);	// Toroljuk a listabol az adott elfogyasztott etelt.
    	position.SetFood(null);		// Toroljuk a mezorol is a kajszagot.
    }
}