package game;
import java.util.*;
import java.util.Random;

public class Ant extends Animal {
	
	public void Move() {
		///Lista az aktualis mezo szomszedairol
	    List<? extends IFieldForAnimalUse> neighbours = position.GetNeighbours();
	    int leftNeighbour;	//bal szomszed szama
	    int rightNeighbour;	//jobb szomszed szama
	    
	    //Menetirany szerinti balfelso szomszed �s jobb felso szomszed megadasa
	    if(this.direction == 0) {
	    	rightNeighbour = 1;
	    	leftNeighbour = 5;
	    }
	    else if(this.direction == 5) {
	    	rightNeighbour = 0;
	        leftNeighbour = 4;
	    }	
	    else {
	    	rightNeighbour = this.direction + 1;
	        leftNeighbour = this.direction - 1;
	    }
	    	
	    ///Seged valtozok felvetele lepes-logikahoz
	    int []chances = new int[3];	//lehetseges celmezok lepes-eselye (1. bal, 2. elore, 3. jobb)
	    int max = 0, place = 0;		//max eselyu mezo
	    int newdir = 0;
	    
	    ///Veletlenszeru szam (1 es 100 kozott) krealasa mozgaslogikahoz, valtozo beallitasok
	    Random randomGenerator = new Random();
	    
	    ///
	    ///Lepeslogika
	    ///
	    for(int i = 0; i < 2; i++) {
	    	//1. iteracio - ha lletezik bal szomszed akkor kiszamoljuk a hozzakapcsolodo eselyeket
			if(i == 0 && neighbours.get(leftNeighbour) != null) {
				if(neighbours.get(leftNeighbour).CanStepOn(this)) {
					chances[i] = randomGenerator.nextInt(100);	//random esely
					if(neighbours.get(leftNeighbour).GetAntIntensity() != 0)
						chances[i] += 5*10;	//ha van hangyaszag, az noveli az eselyt
					chances[i] += (neighbours.get(leftNeighbour).GetFoodIntensity())*1000;//ha van kajaszag, akkor sokkal noveli az eselyt
					if((neighbours.get(leftNeighbour).GetFood() != null)) {
						chances[i] += 999999;
					}
				}	
				else
					chances[i] = 0;	//ha nem lehet ralepni, mert akadaly, akkor az esely is 0
			}
			//2. iteracio - ha elottunk levo szomszed letezik akkor kiszamoljuk a hozzakapcsolodo eselyeket
			else if(i == 1 && neighbours.get(this.direction) != null) {
				if(neighbours.get(this.direction).CanStepOn(this)) {
					chances[i] = randomGenerator.nextInt(100);
					if(neighbours.get(this.direction).GetAntIntensity() != 0)
						chances[i] += 5*10;
					chances[i] += (neighbours.get(this.direction).GetFoodIntensity())*1000;
					if((neighbours.get(this.direction).GetFood() != null)) {
						chances[i] += 999999;
					}
					chances[i] += 40;  ///el�re nagyobb es�llyel megy
				}	
				else
					chances[i] = 0;
			}
			//3. iteracio - ha letezik jobb szomszed akkor kiszamoljuk a hozzakapcsolodo eselyeket
			else if(i == 2 && neighbours.get(rightNeighbour) != null) {
				if(neighbours.get(rightNeighbour).CanStepOn(this)) {
					chances[i] = randomGenerator.nextInt(100);
					if(neighbours.get(rightNeighbour).GetAntIntensity() != 0)
						chances[i] += 5*10;
					chances[i] += (neighbours.get(rightNeighbour).GetFoodIntensity())*1000;
					if((neighbours.get(rightNeighbour).GetFood() != null)) {
						chances[i] += 999999;
					}
				}	
				else
					chances[i] = 0;
			}
	    }
	        
	    //megnezzuk hogy a 3 lehetseges mezo kozul melyikre jutott a legnagyobb esely
	    for(int i = 0; i < 2; i++) {
	    	if(chances[i] > max) {
	    		max = chances[i];
	    		if(i == 0) {
	    			place = leftNeighbour;
	    			newdir = leftNeighbour;
	    		}
	    		else if(i == 1) {
	    			place = this.direction;
	    			newdir = this.direction;
	    		}
	    		else if(i == 2) {
	    			place = rightNeighbour;
	    			newdir = rightNeighbour;
	    		}
	    	}
	    }
	    
	    //a legnagyobbra ralepunk 
	    if(max != 0) { //ha max == 0, akkor valoszinuleg nem leteznek a mezok, vagy az osszes akadaly - maradunk egyhelybe
	    	this.direction = newdir;
	    	this.position.CreateAntSmell();
	    	this.position.StepOff();
	    	
	    	this.position = neighbours.get(place);
	    	neighbours.get(place).StepOn(this);
	    	if(this.position.GetFood() != null) { //ha van rajta kaja,akkor megesszuk
	    		this.position.GetFood().Consume();
	    	}
	    }
	    //ha max == 0, akkor valoszinuleg nem leteznek a mezok, vagy az osszes akadaly
	    //ilyenkor az allat direction-jet 2vel forgatjuk, k�vetkezo korben mar mas mezoket vizsgal - addig forog mig nem lesz jo --> beragadas elkerulese
	    else {
	    	this.direction = (this.direction + 2) % 6;
	    }
	}
    
	public void Kill() {
	   this.engine.DeleteMover(this);	//mozgok listajabol kitorolve
	   position.SetAnimal(null);		//mezorol kitorolve
	   //majd GC begyujti
	}
	
	public long GetId() {
		return this.position.GetId();
	}
}