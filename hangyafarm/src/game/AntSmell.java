package game;

public class AntSmell extends FieldObject implements IUpdatable {
	//T�R�LNI KELL!!! az�rt hagytam benne hogy leforduljon. D�vid
	/*AntSmell() {
		super(null,null);
	}*/
	private int intensity;
	/**
     * Konstruktor az <code>AntSmell</code> inicializ�l�s�hoz.
     * @param p_engine A j�t�kmotor referenci�ja.
     * @param p_position Az a mez�, ahol az objektum l�trej�n.
     * @author Szak�llas D�vid
     */
	AntSmell(IEngineForIngameUse p_engine, IFieldForFieldObjectUse p_position) {
		super(p_engine, p_position);
		position.SetAntSmell(this);
		engine.AddUpdatable(this);
		intensity = 5;
    }

    public void Update() {
    	decrement();
    	if(this.intensity==0)			// Ha 0 az intenzitas, akkor toroljuk a szagot
        	Delete();
    }

    private void decrement() {
    	if(this.intensity>=1)			// Nem lehet negativ!
    		this.intensity--;
    }

    public void SetToMax() {			// Max ertekre allitjuk az intenzitast
        this.intensity=5;
    }

    public void Delete() {
    	position.SetAntSmell(null);		// Toroljuk az adott hangyaszagot a poziciorol
    	engine.DeleteUpdatable(this);	// Toroljuk a hangyaszagot az Updatable listabol
    }

    public int GetIntensity() {
    	return this.intensity;			// Getter metodus az intenzitashoz
    }
}