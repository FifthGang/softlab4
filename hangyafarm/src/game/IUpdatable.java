package game;

public interface IUpdatable {

        public void Update();
}