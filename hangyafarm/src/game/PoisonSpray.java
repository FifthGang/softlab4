package game;

import java.util.List;

public class PoisonSpray extends Spray {
	
	public PoisonSpray(int am) {  //konstruktor  
		this.amount = am;   //a municio kezdeti erteke
		this.isEmpty= false; //letrehozaskor false az erteke, azaz hasznalhato, ha egyszer kifogy, akkor tobbe nem hasznalhato
		}

        public void Refill() {             //a PoisonSpray mennyisegenek novelese
               if(this.isEmpty == false) {	//ha false, akkor meg hasznalhato es ujratoltheto      	   
            	   
            	   this.amount += 5;   //a mereg mennyiseget megnoveli 5 egyseggel
               }
        }

        public void SprayTo(IFieldForSprayUse p_where) {
        	if(this.isEmpty==false) {     //ha az isEmpty false, azt jelenti, hogy van meg municio, lehet fujni
        		
        	   this.amount -= 1; //csokkenti a mereg spray municiojat
        	   if(amount==0) {  //ha a csokkentessel a municio 0 lett, akkor tobbe nem hasznalhato 
        		   this.isEmpty =true;  //isEmpty-t true-ra allitjuk ezzel tobbe nem lesz ujboltoltheto 
        	   }
               p_where.CreatePoison();       //a parameterkent adott helyre merget rak
               
               
            	   
               List<? extends IFieldForSprayUse> neighbours = p_where.GetNeighbours();
               
               for(int i=0;i<neighbours.size();i++){
            	   if(neighbours.get(i) != null) {
            	   neighbours.get(i).CreatePoison(); }//a parameterkent adott hely szomszedaira is merget rak
               }
        
        	
        	}
        }
}