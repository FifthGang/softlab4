package game;

import java.util.ArrayList;
import java.util.List;

import view.GamerFrame;
import view.Window;

public class Engine implements IEngineForIngameUse {
        private List<Field> map;
        private List<IMovable> movers;
        private List<Food> food;
        private List<IUpdatable> updatables;
        private DeoSpray deoSpray;
        private PoisonSpray poisonSpray;
        private int score;
        private boolean isRunning;
        private int Mapm;
        private int Mapn;
        
        private Field checkedGet(int n, int m){
        	if( m< 0 || n <0 || m>= Mapm || n>= Mapn)
        		return null;
        	else return map.get(Mapm*n+m);
        }
        public Engine() {
        	map = new ArrayList<Field>();
            movers = new ArrayList<IMovable>();
            food = new ArrayList<Food>();
            updatables = new ArrayList<IUpdatable>();
            deoSpray = new DeoSpray(100);
        	poisonSpray = new PoisonSpray(25);
            score = 0;
            isRunning = false;
        } 
        public DeoSpray GetDeoSpray() { return deoSpray; }
        public PoisonSpray GetPoisonSpray() { return poisonSpray; }
        public void Start(int m, int n) {
        	
        	deoSpray = new DeoSpray(100);
        	poisonSpray = new PoisonSpray(25);
        	isRunning = true;

     		map = new ArrayList<Field>();

     		Field field = new EmptyField();

     		
     		Mapm = m;
     		Mapn = n;
     		
//     		for(int i=0;i<m*n;i++){			//a palya elemeit betoltom a listbe
//     			field = new EmptyField();
//     			field.SetEngine(this);
//     			field.SetId(i);				//+ id-zes
//     			map.add(field);
//     		}
     		List<Field> sz = null;
     		//------------------D�VID----------------------
     		for(int j= 0; j<Mapn; j++) {
     			for(int i =0; i <Mapm; i++){			//a palya elemeit betoltom a listbe
	     			field = new EmptyField();
	     			field.SetEngine(this);
	     			field.SetId(Mapm*j+i);				//+ id-zes
	     			map.add(field);
     			}
     		}
     		
     		
     		for(int j= 0; j<Mapn; j++) {
     			for(int i =0; i <Mapm; i++){
	     			if(j%2 == 0){
	     				if(i%2 == 0) {
	     					sz = new ArrayList<Field>();
	         				sz.add(checkedGet(j-1,i));
	         				sz.add(checkedGet(j-1,i+1));
	         				sz.add(checkedGet(j,i+1));
	         				sz.add(checkedGet(j+1,i));
	         				sz.add(checkedGet(j,i-1));
	         				sz.add(checkedGet(j-1,i-1));
	         				map.get(Mapm*j+i).SetNeighbours(sz);
	     				}
	     				else {
	     					sz = new ArrayList<Field>();
	     					sz.add(checkedGet(j-1,i));
	         				sz.add(checkedGet(j,i+1));
	         				sz.add(checkedGet(j+1,i+1));
	         				sz.add(checkedGet(j+1,i));
	         				sz.add(checkedGet(j+1,i-1));
	         				sz.add(checkedGet(j,i-1));
	         				map.get(Mapm*j+i).SetNeighbours(sz);
	     				}
	     			}
	     			else{
	     				if(i%2 == 0) {
	     					sz = new ArrayList<Field>();
	         				sz.add(checkedGet(j-1,i));
	         				sz.add(checkedGet(j-1,i+1));
	         				sz.add(checkedGet(j,i+1));
	         				sz.add(checkedGet(j+1,i));
	         				sz.add(checkedGet(j,i-1));
	         				sz.add(checkedGet(j-1,i-1));
	         				map.get(Mapm*j+i).SetNeighbours(sz);
	     				}
	     				else {
	     					sz = new ArrayList<Field>();
	     					sz.add(checkedGet(j-1,i));
	         				sz.add(checkedGet(j,i+1));
	         				sz.add(checkedGet(j+1,i+1));
	         				sz.add(checkedGet(j+1,i));
	         				sz.add(checkedGet(j+1,i-1));
	         				sz.add(checkedGet(j,i-1));
	         				map.get(Mapm*j+i).SetNeighbours(sz);
	     				}	
	     			}
     			}
     		}	
        }
        
        public int GetMapWidth() { return Mapm; }
        
        public void Reset() {
        	map.clear();
            movers.clear();
            food.clear();
            updatables.clear();
            deoSpray = new DeoSpray(100);
            poisonSpray = new PoisonSpray(25);
        }
        
        public void Run() {
        	if(food.isEmpty())
        	{
        		map.clear();
                movers.clear();
                food.clear();
                updatables.clear();
        		if(isRunning==true){
        		GamerFrame frame = new GamerFrame(true,score);
    			frame.setVisible(true);
        		}
        		isRunning = false;
        		
        	}	
        	else
        	{
        		for(int i=0;i<movers.size();i++){
	        		movers.get(i).Move();
	        	}
	        	for(int i=0;i<updatables.size();i++){
	        		updatables.get(i).Update();
	        	}
	        	
	        	score++;
        	}
        }

        public boolean IsRunning() {
        	return isRunning;
        }

        public int GetScore() {
                throw new UnsupportedOperationException();
        }

        public void AddMover(IMovable p_mover) {
               movers.add(p_mover);
        }

        public void DeleteMover(IMovable p_mover) {
                movers.remove(p_mover);
        }

        public void AddUpdatable(IUpdatable p_updatable) {
              updatables.add(p_updatable);
        }

        public void DeleteUpdatable(IUpdatable p_updatable) {
        	updatables.remove(p_updatable);
        }

        public void DeleteFood(Food p_food) {
                food.remove(p_food);
        }

		@Override
		public void AddFood(Food p_food) {
			food.add(p_food);
		}
		
		///////TESZTEL�S EREJ�IG CSAK/////////////
		public void SetMap(List<Field> map) {
			this.map = map;
		}
		public List<Field> GetMap() {
			return this.map;
		}
		public List<IMovable> GetMovers() {
			return this.movers;
		}
		public List<IUpdatable> GetUpdatables() {
			return this.updatables;
		}
		/////////////////////////////////////////
		public void mapInfo(){
			for(int i = 0 ;i < map.size() ; i++){		//ki irja sorba az elemeket
				if(map.get(i).GetAnimal() != null)
					System.out.print(map.get(i).GetId()+"-"+map.get(i).toString()+"-"+map.get(i).GetAnimal().toString()+"\t");
				else if(map.get(i).GetFood() != null)
					System.out.print(map.get(i).GetId()+"-"+map.get(i).toString()+"-"+map.get(i).GetFood().toString()+"\t");
				else if(map.get(i).GetStone() != null)
					System.out.print(map.get(i).GetId()+"-"+map.get(i).toString()+"-"+map.get(i).GetStone().toString()+"\t");
				else if(map.get(i).GetAntSmell() != null)
					System.out.print(map.get(i).GetId()+"-"+map.get(i).toString()+"-"+map.get(i).GetAntSmell().toString()+"\t");
				else if(map.get(i).GetPoison() != null)
					System.out.print(map.get(i).GetId()+"-"+map.get(i).toString()+"-"+map.get(i).GetPoison().toString()+"intensity="+(map.get(i).GetPoison().GetIntensity())+"\t");
				else
					System.out.print(map.get(i).GetId()+"-"+map.get(i).toString()+"\t\t\t");
				if((i+1) % Mapm == 0)
 					System.out.print("\n");
			}
		}
		public void addAnthill(int id){
			List<Field> neighbors;
			int k = 3;
			Anthill	hill = new Anthill();
 			this.AddUpdatable(hill);
 				hill.SetNeighbours(map.get(id).GetNeighbours());
 				for(int i=0;i<6;i++){
 					if(map.get(id).GetNeighbours().get(i)==null)
 						break;
 					else{
 						neighbors = map.get(id).GetNeighbours();
 						neighbors.set(k, hill);
 						map.get(id).GetNeighbours().get(i).SetNeighbours(neighbors);
 						k++;
 						if(k==5)
 							k=0;
 					}
 				}
		}

		public int GetFooD(){
			return food.size();
		}
		public int getpoison(){				//spray figyelok miatti getter
			return poisonSpray.amount;
		}
		public int getdeo(){
			return deoSpray.amount;
		}

}
