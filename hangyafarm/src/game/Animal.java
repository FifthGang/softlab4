package game;

public abstract class Animal implements IMovable {
	protected int direction;
	protected IFieldForAnimalUse position;
	protected IEngineForIngameUse engine;
	
	public abstract void Move();

	public void Kill() {
	}
	
	public int GetDirection() {
		return direction;
	}
	
	public IFieldForAnimalUse getPosition() {
		return position;
	}
}