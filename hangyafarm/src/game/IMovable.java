package game;

public interface IMovable {

        public void Move();

		public long GetId();
}