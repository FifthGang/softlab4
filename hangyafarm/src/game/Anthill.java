package game;

import java.util.List;
import java.util.Random;

public class Anthill extends Field implements IUpdatable {
	
	private int rounds; //hany koronkent tegyen hangyat

	public Anthill(){
		rounds = 0;
	}
        public boolean CanStepOn(Ant p_animal) {
        	return false;
        }
        
        public boolean CanStepOn(AntEater p_animal) {
        	return false;
        }

		public void StepOn(Animal p_animal) {
			throw new UnsupportedOperationException();
		}

        public void StepOff() {
                throw new UnsupportedOperationException();
        }

        public Food GetFood() {
        		return null;
        }

        protected void createAnt() {
    		List<? extends IFieldForAnimalUse> neighbours = GetNeighbours();
        	Ant ant = new Ant();
			ant.engine = this.GetEngine();
			ant.engine.AddMover(ant);
        	Random randomGenerator = new Random();

			ant.direction = randomGenerator.nextInt(6);
			
			while(true) {
				if(neighbours.get(ant.direction) != null && neighbours.get(ant.direction).CanStepOn(ant) != false) {
					neighbours.get(ant.direction).SetAnimal(ant);
    	     		ant.position = neighbours.get(ant.direction);
    	     		break;
				}
				else {
					ant.direction = randomGenerator.nextInt(6);
				}
			}
        }

        public void Update() {
        	rounds++;
        	if(rounds==4){		//1koronkent gyart hangyat
        		rounds = 0;
        		createAnt();
        	}
        	
        }

        public void SetAntSmell(AntSmell p_antsmell) {
                throw new UnsupportedOperationException();
        }


		@Override
		public void SetFood(Food p_food) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void SetStone(Stone p_stone) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Stone GetStone() {
			// TODO Auto-generated method stub
			return null;
		}
}