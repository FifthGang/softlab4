package game;

import java.util.List;
import java.util.Random;

public class AntEater extends Animal {
    private int eatenAnts = 0;	//ehseg szint kezdetben 0
    
	public void Move() {
		int turn = 0;
		
		while(turn <= 1) {
			///Lista az aktualis mezo szomszedairol
		    List<? extends IFieldForAnimalUse> neighbours = position.GetNeighbours();
		    int leftNeighbour;	//bal szomszed szama
		    int rightNeighbour;	//jobb szomszed szama
		    
		    
		    //Menetirany szerinti balfelso szomszed �s jobb felso szomszed megadasa
		    if(this.direction == 0) {
		    	rightNeighbour = 1;
		    	leftNeighbour = 5;
		    }
		    else if(this.direction == 5) {
		    	rightNeighbour = 0;
		        leftNeighbour = 4;
		    }	
		    else {
		    	rightNeighbour = this.direction + 1;
		        leftNeighbour = this.direction - 1;
		    }
		    	
		    ///Seged valtozok felvetele lepes-logikahoz
		    int []chances = new int[3];	//lehetseges celmezok lepes-eselye (1. bal, 2. elore, 3. jobb)
		    int max = 0, place = 0;		//max eselyu mezo
		    int newdir = 0;
		    
		    ///Veletlenszeru szam (1 es 100 kozott) krealasa mozgaslogikahoz, valtozo beallitasok
		    Random randomGenerator = new Random();
		    
		    ///
		    ///Lepeslogika
		    ///
		    for(int i = 0; i < 2; i++) {
		    	//1. iteracio - ha letezik bal szomszed akkor kiszamoljuk a hozzakapcsolodo eselyeket
				if(i == 0 && neighbours.get(leftNeighbour) != null) {
					if(neighbours.get(leftNeighbour).CanStepOn(this)) {
						chances[i] = randomGenerator.nextInt(100);	//random esely
						chances[i] += (neighbours.get(leftNeighbour).GetAntIntensity())*1000;	//ha van hangyaszag, akkor utana eredunk
					}	
					else {	//ha CanStepOn() False-al tart vissza, megnezzuk, hogy hangya miatt-e
						if(neighbours.get(leftNeighbour).GetAnimal() == null)
							chances[i] = 0;	//nincs rajta hangya, csak akadaly lehet, akkor az esely is 0
						else {
							chances[i] = 99999; //van rajta hangya, mindenkep idelepunk, megesszuk a hangyat
							neighbours.get(leftNeighbour).GetAnimal().Kill(); //hangya meghalt
							this.eatenAnts++; //megevett hangyak szama novelve
						}
					}
				}
				//2. iteracio - ha elottunk levo szomszed letezik akkor kiszamoljuk a hozzakapcsolodo eselyeket
				else if(i == 1 && neighbours.get(this.direction) != null) {
					if(neighbours.get(this.direction).CanStepOn(this)) {
						chances[i] = randomGenerator.nextInt(100);
						chances[i] += (neighbours.get(this.direction).GetAntIntensity())*1000;	//ha van hangyaszag, akkor utana eredunk
						chances[i] += 40;  ///elore nagyobb esellyel megy
					}	
					else {	//ha CanStepOn() False-al tart vissza, megnezzuk, hogy hangya miatt-e
						if(neighbours.get(this.direction).GetAnimal() == null)
							chances[i] = 0;	//nincs rajta hangya, csak akadaly lehet, akkor az esely is 0
						else {
							chances[i] = 99999; //van rajta hangya, mindenkep idelepunk, megesszuk a hangyat
							neighbours.get(this.direction).GetAnimal().Kill(); //hangya meghalt
							this.eatenAnts++; //megevett hangyak szama novelve
						}
					}
				}
				//3. iteracio - ha letezik jobb szomszed akkor kiszamoljuk a hozzakapcsolodo eselyeket
				else if(i == 2 && neighbours.get(rightNeighbour) != null) {
					if(neighbours.get(rightNeighbour).CanStepOn(this)) {
						chances[i] = randomGenerator.nextInt(100);
						chances[i] += (neighbours.get(leftNeighbour).GetAntIntensity())*1000;	//ha van hangyaszag, akkor utana eredunk
					}	
					else {	//ha CanStepOn() False-al tart vissza, megnezzuk, hogy hangya miatt-e
						if(neighbours.get(rightNeighbour).GetAnimal() == null)
							chances[i] = 0;	//nincs rajta hangya, csak akadaly lehet, akkor az esely is 0
						else {
							chances[i] = 99999; //van rajta hangya, mindenkep idelepunk, megesszuk a hangyat
							neighbours.get(rightNeighbour).GetAnimal().Kill(); //hangya meghalt
							this.eatenAnts++; //megevett hangyak szama novelve
						}
					}
				}
		    }
		        
		    //megnezzuk hogy jollaktunk-e, ha igen akkor eltununk
		    if(this.eatenAnts == 2) {
		    	this.engine.DeleteMover(this);
		    	this.position.SetAnimal(null);
		    	return;
		    }
		    
		    //ha meg nem laktunk jol:
		    //megnezzuk hogy a 3 lehetseges mezo kozul melyikre jutott a legnagyobb esely
		    for(int i = 0; i < 2; i++) {
		    	if(chances[i] > max) {
		    		max = chances[i];
		    		if(i == 0) {
		    			place = leftNeighbour;
		    			newdir = leftNeighbour;
		    		}
		    		else if(i == 1) {
		    			place = this.direction;
		    			newdir = this.direction;
		    		}
		    		else if(i == 2) {
		    			place = rightNeighbour;
		    			newdir = rightNeighbour;
		    		}
		    	}
		    }
		    
		    //a legnagyobbra ralepunk 
		    if(max != 0) { 
		    	this.direction = newdir;
		    	this.position.StepOff();
		    	this.position = neighbours.get(place);
		    	neighbours.get(place).StepOn(this);
		    }
		    //ha max == 0, akkor valoszinuleg nem leteznek a mezok, vagy az osszes akadaly
		    //ilyenkor az allat direction-jet 2vel forgatjuk, k�vetkezo korben mar mas mezoket vizsgal - addig forog mig nem lesz jo --> beragadas elkerulese
		    else {
		    	this.direction = (this.direction + 2) % 6;
		    }
		    
		    turn++;
		}
	}
	public void SetEngine(IEngineForIngameUse engine)  
	{
		this.engine=engine;
	}
	
	
	public void SetPosition(IFieldForAnimalUse position)  
	{
		this.position=position;
	}
	
	public IEngineForIngameUse GetEngine ()  
	{
		return this.engine;
	}

	public void Kill() { //ures kill
	        return;
	}
		
	public long GetId() {
		return this.position.GetId();
	}
}